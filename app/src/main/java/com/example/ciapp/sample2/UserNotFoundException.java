package com.example.ciapp.sample2;

public class UserNotFoundException extends Exception {
    public UserNotFoundException(){super("User does not exists");}
}
