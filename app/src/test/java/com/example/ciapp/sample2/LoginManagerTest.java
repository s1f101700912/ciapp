package com.example.ciapp.sample2;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class LoginManagerTest {
    private LoginManager loginManager;

    @Before
    public void setUp() throws ValidateFailedException{
        loginManager = new LoginManager();
        loginManager.register("testUser1", "Password");
    }

    @Test
    public void testLoginSuccess() throws LoginFailedException,UserNotFoundException  {
        User user = loginManager.login("testUser1", "Password");
        assertThat(user.getUsername(), is("testUser1"));
        assertThat(user.getPassword(), is("Password"));
    }

    @Test(expected = LoginFailedException.class)
    public void testLoginWrongPassword() throws LoginFailedException,UserNotFoundException  {
        User user = loginManager.login("testUser1", "1234");
    }

    @Test(expected = UserNotFoundException.class)
    public void testLoginUnregisteredUser() throws LoginFailedException,UserNotFoundException {
        User user = loginManager.login("iniad", "password");
    }

}